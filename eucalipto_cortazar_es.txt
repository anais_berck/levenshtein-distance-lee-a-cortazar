Un fama anda por el bosque y aunque no necesita leña mira codiciosamente los árboles. Los árboles tienen un miedo terrible porque conocen las costumbres de los famas y temen lo peor. En medio de todos está un eucalipto hermoso, y el fama al verlo da un grito de alegría y baila tregua y baila catala en torno del perturbado eucalipto, diciendo así:

– Hojas antisépticas, invierno con salud, gran higiene. 

Saca un hacha y golpea al eucalipto en el estómago, sin importársele nada. El eucalipto gime, herido de muerte, y los otros árboles oyen que dice entre suspiros:

– Pensar que este imbécil no tenía más que comprarse unas pastillas Valda.





