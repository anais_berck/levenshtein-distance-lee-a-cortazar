#!/usr/bin/env/ python

#    This is the webinterface for 'la_distancia_de_levenshtein_lee_a_cortazar'
#    and generates the pdf using weasyprint.

#    Copyright (C) 2021, Anais Berck
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

import textwrap
from io import StringIO
from os.path import dirname
import re
import datetime

import os
from fcntl import lockf, LOCK_EX, LOCK_UN

from flask import Flask, Response, render_template, request, send_file, session
from weasyprint import HTML

from la_distancia_de_levenshtein_lee_a_cortazar_es import (
    calculate_distances as calculate_distances_es,
    find_nearest_species as find_nearest_species_es,
    generate_in_between_species as generate_in_between_species_es,
    generate_new_fragment as generate_new_fragment_es,
    openfile as openfile_es,
    print_map as print_map_es,
    print_table as print_table_es,
    sort_distances as sort_distances_es)

from la_distancia_de_levenshtein_lee_a_cortazar_en import (
    calculate_distances as calculate_distances_en,
    find_nearest_species as find_nearest_species_en,
    generate_in_between_species as generate_in_between_species_en,
    generate_new_fragment as generate_new_fragment_en,
    openfile as openfile_en,
    print_map as print_map_en,
    print_table as print_table_en,
    sort_distances as sort_distances_en)

from settings import BASEURL, DEFAULT_LANGUAGE, SECRET_KEY

COUNTER_PATH_ES = 'edition_counter.txt'
COUNTER_PATH_EN = 'edition_counter_en.txt'

def strip_but_spaces_and_words (text):
	return re.sub(r'[^\w\s\d]', '', text)

def wrap (text, width):
  return'\n'.join(['\n'.join(textwrap.wrap(line, width=width)) for line in text.splitlines()])

def read_sources (*paths):
  return [ (p, wrap(open(p, 'r').read(), 120)) for p in paths ]

def get_language():
  if 'LANGUAGE' in session:
    return session['LANGUAGE']
  else:
    return DEFAULT_LANGUAGE

def set_language(language):
  session['LANGUAGE'] = language
  session.modified = True

def get_edition_count_es():
  fd = os.open(COUNTER_PATH_ES, os.O_RDWR|os.O_CREAT)
  lockf(fd, LOCK_EX)
  fo = os.fdopen(fd, 'r+', encoding='utf-8')
  content = fo.read()
  if not content:
    edition_count = 0
  else:
    edition_count = int(content.strip())
  edition_count += 1
  fo.seek(0)
  fo.truncate()
  fo.write(str(edition_count))
  fo.flush()
  lockf(fd, LOCK_UN)
  os.close(fd)

  return edition_count

def get_edition_count_en():
  fd = os.open(COUNTER_PATH_EN, os.O_RDWR|os.O_CREAT)
  lockf(fd, LOCK_EX)
  fo = os.fdopen(fd, 'r+', encoding='utf-8')
  content = fo.read()
  if not content:
    edition_count = 0
  else:
    edition_count = int(content.strip())
  edition_count += 1
  fo.seek(0)
  fo.truncate()
  fo.write(str(edition_count))
  fo.flush()
  lockf(fd, LOCK_UN)
  os.close(fd)

  return edition_count

app = Flask(__name__)
app.secret_key = SECRET_KEY
trees_es = []
trees_en = []

## 1A. Open textfiles & turn textfiles in machine readable lists

# Cortazar
txt_es = 'eucalipto_cortazar_es.txt'
txt_en = 'eucalipto_cortazar_en.txt'

all_plural_es = openfile_es('arboles_plural_es.txt')
all_simple_es = openfile_es('arboles_simple_es.txt')

all_plural_en = openfile_en('arboles_plural_en.txt')
all_simple_en = openfile_en('arboles_simple_en.txt')


## 1B. Turn list of trees into dictionary of single/plural words
trees_es = dict(zip(all_simple_es, all_plural_es))
trees_en = dict(zip(all_simple_en, all_plural_en))

## 2. HOMEPAGE
## laod fragment Cortazar
with open(txt_es, 'r') as source:
  fragment_es = source.read()

with open(txt_en, 'r') as source:
  fragment_en = source.read()

# cover_distances = sort_distances(calculate_distances('eucalipto', all_simple))

fragment_words_es = re.split(r'[\n\s]+', strip_but_spaces_and_words(fragment_es).lower())
fragment_word_distances_es = sort_distances_es(calculate_distances_es('eucalipto', fragment_words_es))

fragment_words_en = re.split(r'[\n\s]+', strip_but_spaces_and_words(fragment_en).lower())
fragment_word_distances_en = sort_distances_es(calculate_distances_es('eucalipto', fragment_words_en))

@app.route('{}/en'.format(BASEURL))
def en():
  set_language('en')
  return index()
  
@app.route('{}/es'.format(BASEURL))
def es():
  set_language('es')
  return index()

@app.route('{}/'.format(BASEURL))
def index():
  if get_language() == 'es':
    return index_es()
  else:
    return index_en()

def index_es():
  return render_template('index_es.html', trees=trees_es, BASEURL=BASEURL)

def index_en():
  return render_template('index_en.html', trees=trees_en, BASEURL=BASEURL)


@app.route('{}/generate'.format(BASEURL), methods=['POST'])
def generate():
  if get_language() == 'es':
    return generate_es()
  else:
    return generate_en()


def generate_es():
  edition_count = get_edition_count_es()
  new_main_tree = str(request.form['selected_tree'])

  if new_main_tree in all_simple_es:
    
    # Generate map for the cover
    # cover_map = StringIO()
    # print_map(cover_distances, show_distances=False, file_out=cover_map)

    fragment_cover_map = StringIO()
    print_map_es(fragment_word_distances_es, show_distances=False, file_out=fragment_cover_map)

    ## 3. Count the similarity between the main tree and the other species in the forest
    similarities = calculate_distances_es(new_main_tree, all_simple_es)
    
    ## 4. Sort the similarities between the trees and the new main tree from the lowest to the highest counts
    sorted_similarities = sort_distances_es(similarities)

    # Find the nearest species
    near_species, nearest_species = find_nearest_species_es(sorted_similarities, trees_es)

    new_fragment = generate_new_fragment_es(fragment_es, new_main_tree, nearest_species)

    ## 5. Compare the similarity between the main character tree and the main species in the forest, 
    repetitive_poetry = StringIO()
    in_between_species = generate_in_between_species_es(new_main_tree, near_species, file_out=repetitive_poetry)

    forest_map = StringIO()
    print_map_es(sorted_similarities, file_out=forest_map)

    table_of_intermediary_species = StringIO()
    print_table_es(new_main_tree, near_species, in_between_species, table_of_intermediary_species)
    
    now = datetime.datetime.now()

    context = {
      'edition_count': edition_count,
      # 'cover_map': cover_map.getvalue(),
      'date': now.strftime('%d-%m-%Y'),
      'time': now.strftime('%H:%M:%S'),
      'fragment_cover_map': fragment_cover_map.getvalue(),
      'forest_map': forest_map.getvalue(),
      'new_fragment': wrap(new_fragment, 85),
      'repetitive_poetry': wrap(repetitive_poetry.getvalue(), 55),
      'table_of_intermediary_species': table_of_intermediary_species.getvalue(),
      'sources': read_sources('la_distancia_de_levenshtein_lee_a_cortazar_es.py'),
      'BASEDIR': dirname(__file__)
    }

    raw_html = render_template('print_es.html', **context)
    pdf = HTML(string=raw_html).write_pdf()

    repetitive_poetry.close()
    forest_map.close()
    table_of_intermediary_species.close()

    # return send_file(pdf, attachment_filename='La distancia de Levenshtein {}.pdf'.format(edition_count), as_attachment=True)

    r = Response(pdf, mimetype='application/pdf')

    r.headers.extend({
      'Content-Disposition': 'attachment; filename="La distancia de Levenshtein {}.pdf"'.format(edition_count)
    })

    return r

  return '500'


def generate_en():
  edition_count = get_edition_count_en()
  new_main_tree = str(request.form['selected_tree'])

  if new_main_tree in all_simple_en:
    
    # Generate map for the cover
    # cover_map = StringIO()
    # print_map(cover_distances, show_distances=False, file_out=cover_map)

    fragment_cover_map = StringIO()
    print_map_en(fragment_word_distances_en, show_distances=False, file_out=fragment_cover_map)

    ## 3. Count the similarity between the main tree and the other species in the forest
    similarities = calculate_distances_en(new_main_tree, all_simple_en)
    
    ## 4. Sort the similarities between the trees and the new main tree from the lowest to the highest counts
    sorted_similarities = sort_distances_en(similarities)

    # Find the nearest species
    near_species, nearest_species = find_nearest_species_en(sorted_similarities, trees_en)

    new_fragment = generate_new_fragment_en(fragment_en, new_main_tree, nearest_species)

    ## 5. Compare the similarity between the main character tree and the main species in the forest, 
    repetitive_poetry = StringIO()
    in_between_species = generate_in_between_species_en(new_main_tree, near_species, file_out=repetitive_poetry)

    forest_map = StringIO()
    print_map_en(sorted_similarities, file_out=forest_map)

    table_of_intermediary_species = StringIO()
    print_table_en(new_main_tree, near_species, in_between_species, table_of_intermediary_species)
    
    now = datetime.datetime.now()

    context = {
      'edition_count': edition_count,
      # 'cover_map': cover_map.getvalue(),
      'date': now.strftime('%d-%m-%Y'),
      'time': now.strftime('%H:%M:%S'),
      'fragment_cover_map': fragment_cover_map.getvalue(),
      'forest_map': forest_map.getvalue(),
      'new_fragment': wrap(new_fragment, 85),
      'repetitive_poetry': wrap(repetitive_poetry.getvalue(), 55),
      'table_of_intermediary_species': table_of_intermediary_species.getvalue(),
      'sources': read_sources('la_distancia_de_levenshtein_lee_a_cortazar_en.py'),
      'BASEDIR': dirname(__file__)
    }

    raw_html = render_template('print_en.html', **context)
    pdf = HTML(string=raw_html).write_pdf()

    repetitive_poetry.close()
    forest_map.close()
    table_of_intermediary_species.close()

    # return send_file(pdf, attachment_filename='La distancia de Levenshtein {}.pdf'.format(edition_count), as_attachment=True)

    r = Response(pdf, mimetype='application/pdf')

    r.headers.extend({
      'Content-Disposition': 'attachment; filename="Levenshtein Distance reads Cortázar {}.pdf"'.format(edition_count)
    })

    return r

  return '500'